package com.example.tpandroid;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.example.tpandroid.adapter.AdapterListeMovie;
import com.example.tpandroid.model.GlobalJson;
import com.example.tpandroid.model.MovieShowTimes;
import com.example.tpandroid.rest.ApiHelper;

public  class MainActivity extends AppCompatActivity implements AdapterListeMovie.OnMovieClickedListener  {

    private GlobalJson globalJson;
    private static String  TAG= MainActivity.class.getSimpleName();
    private RecyclerView recyclerViewMovie;
    public static final String EXTRA_MOVIE= "com.example.tpandroid.MOVIE";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        recyclerViewMovie = findViewById(R.id.recyclerViewMovie);
        ApiHelper.getInstance().getApiMovie().getMovie().enqueue(new Callback<GlobalJson>(){
            @Override
            public void onResponse(Call<GlobalJson> call, Response<GlobalJson> response)
            {
                if (response.isSuccessful()) {
                    globalJson = response.body();

                    // A FINIR SET ADAPTER
                    recyclerViewMovie.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
                    recyclerViewMovie.setAdapter(new AdapterListeMovie(globalJson.movieShowtimes, MainActivity.this, getApplicationContext()));
                    Log.d(TAG, globalJson.movieShowtimes.get(0).onShow.movie.title);
                }
                else {
                    Log.e(TAG, "La réponse httpS n'est pas compris entre 200 et 300 inclus");
                }
            }

            @Override
            public void onFailure(Call<GlobalJson> call, Throwable t) {
                Log.e(TAG, "Pb connexion internet" + t.getMessage());
                t.printStackTrace();
            }
        });
    }

    @Override
    public void onMovieClicked(MovieShowTimes movie) {
       Log.d(TAG, movie.onShow.movie.title);
        Intent intent = new Intent(this, DetailFilmActivity.class);
        intent.putExtra(EXTRA_MOVIE, movie);
        startActivity(intent);
    }
}
