package com.example.tpandroid.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.tpandroid.MainActivity;
import com.example.tpandroid.R;
import com.example.tpandroid.model.GlobalJson;
import com.example.tpandroid.model.MovieShowTimes;
import com.squareup.picasso.Picasso;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;
import androidx.recyclerview.widget.RecyclerView;

import androidx.annotation.NonNull;

public class AdapterListeMovie extends RecyclerView.Adapter<AdapterListeMovie.MovieViewHolder> {

        private ArrayList<MovieShowTimes> movies;
        private OnMovieClickedListener listener;
        private Context context;

        public AdapterListeMovie(ArrayList<MovieShowTimes> movies, OnMovieClickedListener onMovieClickedListener, Context context) {
            this.movies = movies;
            this.listener = onMovieClickedListener;
            this.context = context;
        }

        @NonNull
        @Override
        public MovieViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_movie, parent, false);
            return new MovieViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull MovieViewHolder holder, int position) {
            MovieShowTimes movie = movies.get(position);
            holder.textViewTitle.setText(movie.onShow.movie.title);
            holder.textViewMark.setText(String.valueOf(movie.onShow.movie.statistics.userRating));
            Picasso.with(context).load(movie.onShow.movie.poster.href).into(holder.imageViewScreen);
            holder.itemView.setOnClickListener(v -> listener.onMovieClicked(movie));
        }

        @Override
        public int getItemCount() {
            return movies.size();
        }

        static class MovieViewHolder extends RecyclerView.ViewHolder {
            TextView textViewTitle;
            TextView textViewMark;
            ImageView imageViewScreen;

            MovieViewHolder(View view) {
                super(view);
                textViewTitle = view.findViewById(R.id.textViewTitle);
                textViewMark = view.findViewById(R.id.textViewMark);
                imageViewScreen = view.findViewById(R.id.imageViewScreen);
            }
        }

        public interface OnMovieClickedListener {
            void onMovieClicked(MovieShowTimes movie);
        }
}
