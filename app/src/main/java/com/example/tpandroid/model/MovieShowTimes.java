package com.example.tpandroid.model;

import java.io.Serializable;

public class MovieShowTimes implements Serializable {
    public OnShow onShow;
    public String display;
}
