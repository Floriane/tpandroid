package com.example.tpandroid.model;

import java.io.Serializable;
import java.util.ArrayList;

public class Movie implements Serializable {
    public String title;
    public ArrayList<Genre> genre;
    public Statistics statistics;
    public Poster poster;
    public Trailer trailer;

}
