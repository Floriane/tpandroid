package com.example.tpandroid;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.tpandroid.model.MovieShowTimes;
import com.squareup.picasso.Picasso;

public class DetailFilmActivity extends AppCompatActivity {

    private TextView titreTextView;
    private TextView genreTextView;
    private ImageView imageViewScreen;
    private Button buttonPlay;
    private MovieShowTimes movie;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_film);
        titreTextView = findViewById(R.id.textViewTitle);
        genreTextView = findViewById(R.id.textViewGenre);
        imageViewScreen = findViewById(R.id.imageViewScreen);
        buttonPlay = findViewById(R.id.buttonPlay);

        Intent intent = getIntent();
        movie = (MovieShowTimes) intent.getSerializableExtra(MainActivity.EXTRA_MOVIE);

        titreTextView.setText(movie.onShow.movie.title);
        genreTextView.setText(movie.onShow.movie.genre.get(0).name);
        Picasso.with(getApplicationContext()).load(movie.onShow.movie.poster.href).into(imageViewScreen);

        if(movie.onShow.movie.trailer == null || movie.onShow.movie.trailer.href == null)
        {
            buttonPlay.setVisibility(View.GONE);
        }
        else
        {
            buttonPlay.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    Intent intent = new Intent(Intent.ACTION_VIEW , Uri.parse(movie.onShow.movie.trailer.href));
                    startActivity(intent);
                }
            });
        }
    }
}
