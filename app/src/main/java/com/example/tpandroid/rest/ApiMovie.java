package com.example.tpandroid.rest;

import com.example.tpandroid.model.GlobalJson;

import retrofit2.Call;
import retrofit2.http.GET;
public interface ApiMovie {

    @GET("pam/cine.json")
    Call<GlobalJson> getMovie();

}
