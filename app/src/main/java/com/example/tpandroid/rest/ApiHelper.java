package com.example.tpandroid.rest;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiHelper {
    private ApiMovie apiMovie;

    public ApiMovie getApiMovie() {
        return apiMovie;
    }

    private ApiHelper() {
        Retrofit retrofit = new Retrofit.Builder().baseUrl("https://etudiants.openium.fr/").addConverterFactory(GsonConverterFactory.create()).build();
        apiMovie = retrofit.create(ApiMovie.class);
    }

    private static volatile ApiHelper instance;


    public static synchronized ApiHelper getInstance() {
        if (instance == null) {
            instance = new ApiHelper();
        }
        return instance;
    }
}